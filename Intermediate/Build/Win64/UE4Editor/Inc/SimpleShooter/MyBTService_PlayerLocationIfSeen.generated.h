// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_MyBTService_PlayerLocationIfSeen_generated_h
#error "MyBTService_PlayerLocationIfSeen.generated.h already included, missing '#pragma once' in MyBTService_PlayerLocationIfSeen.h"
#endif
#define SIMPLESHOOTER_MyBTService_PlayerLocationIfSeen_generated_h

#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_SPARSE_DATA
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyBTService_PlayerLocationIfSeen(); \
	friend struct Z_Construct_UClass_UMyBTService_PlayerLocationIfSeen_Statics; \
public: \
	DECLARE_CLASS(UMyBTService_PlayerLocationIfSeen, UBTService_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UMyBTService_PlayerLocationIfSeen)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyBTService_PlayerLocationIfSeen(); \
	friend struct Z_Construct_UClass_UMyBTService_PlayerLocationIfSeen_Statics; \
public: \
	DECLARE_CLASS(UMyBTService_PlayerLocationIfSeen, UBTService_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UMyBTService_PlayerLocationIfSeen)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyBTService_PlayerLocationIfSeen(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBTService_PlayerLocationIfSeen) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBTService_PlayerLocationIfSeen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBTService_PlayerLocationIfSeen); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBTService_PlayerLocationIfSeen(UMyBTService_PlayerLocationIfSeen&&); \
	NO_API UMyBTService_PlayerLocationIfSeen(const UMyBTService_PlayerLocationIfSeen&); \
public:


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBTService_PlayerLocationIfSeen(UMyBTService_PlayerLocationIfSeen&&); \
	NO_API UMyBTService_PlayerLocationIfSeen(const UMyBTService_PlayerLocationIfSeen&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBTService_PlayerLocationIfSeen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBTService_PlayerLocationIfSeen); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMyBTService_PlayerLocationIfSeen)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_12_PROLOG
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_INCLASS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_INCLASS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class UMyBTService_PlayerLocationIfSeen>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_MyBTService_PlayerLocationIfSeen_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
