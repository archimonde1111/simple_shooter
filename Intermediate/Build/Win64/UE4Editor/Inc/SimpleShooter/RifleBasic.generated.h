// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_RifleBasic_generated_h
#error "RifleBasic.generated.h already included, missing '#pragma once' in RifleBasic.h"
#endif
#define SIMPLESHOOTER_RifleBasic_generated_h

#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_SPARSE_DATA
#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_RPC_WRAPPERS
#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARifleBasic(); \
	friend struct Z_Construct_UClass_ARifleBasic_Statics; \
public: \
	DECLARE_CLASS(ARifleBasic, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(ARifleBasic)


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARifleBasic(); \
	friend struct Z_Construct_UClass_ARifleBasic_Statics; \
public: \
	DECLARE_CLASS(ARifleBasic, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(ARifleBasic)


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARifleBasic(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARifleBasic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARifleBasic); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARifleBasic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARifleBasic(ARifleBasic&&); \
	NO_API ARifleBasic(const ARifleBasic&); \
public:


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARifleBasic(ARifleBasic&&); \
	NO_API ARifleBasic(const ARifleBasic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARifleBasic); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARifleBasic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARifleBasic)


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Root() { return STRUCT_OFFSET(ARifleBasic, Root); } \
	FORCEINLINE static uint32 __PPO__RifleMesh() { return STRUCT_OFFSET(ARifleBasic, RifleMesh); } \
	FORCEINLINE static uint32 __PPO__MuzzleFlash() { return STRUCT_OFFSET(ARifleBasic, MuzzleFlash); } \
	FORCEINLINE static uint32 __PPO__HitFlash() { return STRUCT_OFFSET(ARifleBasic, HitFlash); } \
	FORCEINLINE static uint32 __PPO__MuzzleSound() { return STRUCT_OFFSET(ARifleBasic, MuzzleSound); } \
	FORCEINLINE static uint32 __PPO__ImpactSound() { return STRUCT_OFFSET(ARifleBasic, ImpactSound); } \
	FORCEINLINE static uint32 __PPO__MaxRange() { return STRUCT_OFFSET(ARifleBasic, MaxRange); } \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(ARifleBasic, Damage); }


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_12_PROLOG
#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_RPC_WRAPPERS \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_INCLASS \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_INCLASS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class ARifleBasic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SimpleShooter_Source_SimpleShooter_Public_Actors_RifleBasic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
