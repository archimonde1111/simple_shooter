// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLESHOOTER_BTTask_ClearKeyValue_generated_h
#error "BTTask_ClearKeyValue.generated.h already included, missing '#pragma once' in BTTask_ClearKeyValue.h"
#endif
#define SIMPLESHOOTER_BTTask_ClearKeyValue_generated_h

#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_SPARSE_DATA
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_RPC_WRAPPERS
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_ClearKeyValue(); \
	friend struct Z_Construct_UClass_UBTTask_ClearKeyValue_Statics; \
public: \
	DECLARE_CLASS(UBTTask_ClearKeyValue, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_ClearKeyValue)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_ClearKeyValue(); \
	friend struct Z_Construct_UClass_UBTTask_ClearKeyValue_Statics; \
public: \
	DECLARE_CLASS(UBTTask_ClearKeyValue, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SimpleShooter"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_ClearKeyValue)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_ClearKeyValue(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_ClearKeyValue) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_ClearKeyValue); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_ClearKeyValue); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_ClearKeyValue(UBTTask_ClearKeyValue&&); \
	NO_API UBTTask_ClearKeyValue(const UBTTask_ClearKeyValue&); \
public:


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_ClearKeyValue(UBTTask_ClearKeyValue&&); \
	NO_API UBTTask_ClearKeyValue(const UBTTask_ClearKeyValue&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_ClearKeyValue); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_ClearKeyValue); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTTask_ClearKeyValue)


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_PRIVATE_PROPERTY_OFFSET
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_12_PROLOG
#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_RPC_WRAPPERS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_INCLASS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_PRIVATE_PROPERTY_OFFSET \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_SPARSE_DATA \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_INCLASS_NO_PURE_DECLS \
	SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLESHOOTER_API UClass* StaticClass<class UBTTask_ClearKeyValue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SimpleShooter_Source_SimpleShooter_Private_BlackboardTasks_BTTask_ClearKeyValue_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
