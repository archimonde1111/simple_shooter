// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class ARifleBasic;

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
		float MaxHealth = 100;
	UPROPERTY(VisibleAnywhere)
		float Health;

	void Deal_Damage(float Damage);

	void MoveForward(float AxisValue);
	void LookUp(float AxisValue);
	void LookAround(float AxisValue);
	void MoveRight(float AxisValue);

	UPROPERTY(EditDefaultsOnly)
		USpringArmComponent* SpringArm;
	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* Camera;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ARifleBasic> RifleClass;
	UPROPERTY()
		ARifleBasic* Rifle;

public:
	AShooterCharacter();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Tick(float DeltaTime) override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
		FORCEINLINE bool Is_Dead() const { return Health <= 0; }
	UFUNCTION(BlueprintCallable)
		FORCEINLINE float Get_Health_Prercent() const { return Health/MaxHealth; }

protected:
	virtual void BeginPlay() override;

public:
	void Fire_Weapon();
};
