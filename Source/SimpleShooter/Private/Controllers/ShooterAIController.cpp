// Fill out your copyright notice in the Description page of Project Settings.


#include "Controllers/ShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "../ShooterCharacter.h"


void AShooterAIController::BeginPlay()
{
    Super::BeginPlay();

    if(!AIBehavior) { return; };

    RunBehaviorTree(AIBehavior);
    PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    
    if(!GetPawn()) { return; }
    if(!GetBlackboardComponent()) { return; }

    GetBlackboardComponent()->SetValueAsVector(FName("StartLocation"), GetPawn()->GetActorLocation());
}

void AShooterAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}


bool AShooterAIController::Is_Dead()
{
    AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter> (GetPawn());
    if(!ControlledCharacter) { return true; }

    return ControlledCharacter->Is_Dead();
}