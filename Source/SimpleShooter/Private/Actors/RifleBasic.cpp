// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/RifleBasic.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


ARifleBasic::ARifleBasic()
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent> (FName("RootComp"));
	SetRootComponent(Root);
	RifleMesh = CreateDefaultSubobject<USkeletalMeshComponent> (FName("RifleMesh"));

	RifleMesh->SetupAttachment(Root);
}

void ARifleBasic::BeginPlay()
{
	Super::BeginPlay();
	

}

void ARifleBasic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ARifleBasic::Trigger_Pulled()
{
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, RifleMesh, FName("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, RifleMesh, FName("MuzzleFlashSocket"));

	FHitResult HitResult;
	FVector ShotDirection;
	if(Gun_Trace(HitResult, ShotDirection))
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFlash, HitResult.Location, ShotDirection.Rotation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, HitResult.Location, ShotDirection.Rotation());

		FPointDamageEvent PointDamageEvent(Damage, HitResult, ShotDirection, nullptr);
		AActor* HittedActor = HitResult.GetActor();
		if(HittedActor == nullptr) { return; };

		HittedActor->TakeDamage(Damage, PointDamageEvent, Get_OwnerController(), this);//check GetOwner()->GetInstigatorController()
	}
}
bool ARifleBasic::Gun_Trace(FHitResult& OutHitResult, FVector& ShotDirection)
{
	if(!Get_OwnerController()) { return false; }

	FVector CameraLocation;
	FRotator CameraRotation;
	Get_OwnerController()->GetPlayerViewPoint(CameraLocation, CameraRotation);

	FVector End = CameraLocation + CameraRotation.Vector() * MaxRange;
	ShotDirection = -CameraRotation.Vector();
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	return GetWorld()->LineTraceSingleByChannel(OutHitResult, CameraLocation, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}


AController* ARifleBasic::Get_OwnerController() const
{
	APawn* OwnerPawn = Cast<APawn> (GetOwner());
	if(OwnerPawn == nullptr) return nullptr;

	return OwnerPawn->GetController();
}