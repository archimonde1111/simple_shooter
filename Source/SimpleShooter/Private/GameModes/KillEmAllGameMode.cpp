// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "Controllers/ShooterAIController.h"


void AKillEmAllGameMode::Pawn_Killed(APawn* KilledPawn)
{
    Super::Pawn_Killed(KilledPawn);
    
    APlayerController* PlayerController = Cast<APlayerController> (KilledPawn->GetController());
    if(PlayerController)
    {
        End_Game(false);
    }

    for (AShooterAIController* AIController : TActorRange<AShooterAIController> (GetWorld()))
    {
        if(!AIController->Is_Dead())
        {
            return;
        }
    }
    End_Game(true);
}

void AKillEmAllGameMode::End_Game(bool bIsPlayerWinner)
{
     for (AController* Controller : TActorRange<AController> (GetWorld()))
     {
        bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;

        Controller->(Controller->GetPawn(), bIsWinner);
     }
}