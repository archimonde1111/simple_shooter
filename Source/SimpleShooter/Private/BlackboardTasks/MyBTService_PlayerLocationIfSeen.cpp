// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTService_PlayerLocationIfSeen.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"


UMyBTService_PlayerLocationIfSeen::UMyBTService_PlayerLocationIfSeen()
{
    NodeName = TEXT("Update Player Location");
}

void UMyBTService_PlayerLocationIfSeen::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
    if(!OwnerComp.GetBlackboardComponent()) { return; }
    if(!OwnerComp.GetAIOwner()) { return; }

    APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
    if(!PlayerPawn) { return; }

    bool bSeesPlayer = OwnerComp.GetAIOwner()->LineOfSightTo(PlayerPawn);
    if(bSeesPlayer)
    {
        OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), PlayerPawn);
    }
    else
    {
        OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
    }
}