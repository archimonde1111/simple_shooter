// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Shoot.h"
#include "AIController.h"
#include "../ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"


UBTTask_Shoot::UBTTask_Shoot()
{
    NodeName = TEXT("Fire Weapon");
}


EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    if(!OwnerComp.GetAIOwner()) { return EBTNodeResult::Failed; }
    AShooterCharacter* OwningActor = Cast<AShooterCharacter> (OwnerComp.GetAIOwner()->GetPawn());

    if(!OwningActor) { return EBTNodeResult::Failed; }
    OwningActor->Fire_Weapon();

    return EBTNodeResult::Succeeded;
}