// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RifleBasic.generated.h"

class USceneComponent;
class USkeletalMeshComponent;

UCLASS()
class SIMPLESHOOTER_API ARifleBasic : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* RifleMesh;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* MuzzleFlash;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* HitFlash;
	UPROPERTY(EditDefaultsOnly)
		USoundBase* MuzzleSound;
	UPROPERTY(EditDefaultsOnly)
		USoundBase* ImpactSound;

	UPROPERTY(EditAnywhere)
		float MaxRange = 10000;

	UPROPERTY(EditAnywhere)
		int32 Damage = 50;


	bool Gun_Trace(FHitResult& OutHitResult, FVector& ShotDirection);


	AController* Get_OwnerController() const;

protected:
	virtual void BeginPlay() override;


public:	
	ARifleBasic();
	virtual void Tick(float DeltaTime) override;

	void Trigger_Pulled();
};
