// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API AShooterAIController : public AAIController
{
	GENERATED_BODY()

	APawn* PlayerPawn = nullptr;
	
protected:
	virtual void BeginPlay() override;

public: 
	virtual void Tick(float DeltaTime) override;

	bool Is_Dead();
private:

	UPROPERTY(EditAnyWhere, Category = "AI")
		class UBehaviorTree* AIBehavior = nullptr;

};
