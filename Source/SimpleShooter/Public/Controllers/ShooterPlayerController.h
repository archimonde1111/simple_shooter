// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"


class UUserWidget;

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
		float RestartDelay = 5.f;
	FTimerHandle RestartTimer;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> LoseScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> WinScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> HUDClass;

	UPROPERTY()
		UUserWidget* HUD = nullptr;

public:
	virtual void BeginPlay() override;
	virtual void GameHasEnded(AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;
};
