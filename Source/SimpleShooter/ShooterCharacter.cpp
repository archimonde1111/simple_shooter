// archimonde1111


#include "ShooterCharacter.h"
#include "Components/InputComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Actors/RifleBasic.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooterGameModeBase.h"

AShooterCharacter::AShooterCharacter()
{
	PrimaryActorTick.bCanEverTick = false;

	SpringArm = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArm"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->bUsePawnControlRotation = true;
	Camera = CreateDefaultSubobject<UCameraComponent> (FName("Camera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	
}

void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;
	
	Rifle = GetWorld()->SpawnActor<ARifleBasic> (RifleClass);
	Rifle->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("Weapon_Socket"));
	Rifle->SetOwner(this);
	GetMesh()->HideBoneByName(FName("weapon_r"), EPhysBodyOp::PBO_None);
}

void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(FName("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(FName("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(FName("LookUp"), this, &AShooterCharacter::LookUp);
	PlayerInputComponent->BindAxis(FName("LookRight"), this, &AShooterCharacter::LookAround);
	PlayerInputComponent->BindAction(FName("Jump"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Jump);

	PlayerInputComponent->BindAction(FName("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Fire_Weapon);
}
void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}
void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}
void AShooterCharacter::LookUp(float AxisValue)
{
	AddControllerPitchInput(AxisValue);
}
void AShooterCharacter::LookAround(float AxisValue)
{
	AddControllerYawInput(AxisValue);
}

void AShooterCharacter::Fire_Weapon()
{
	if(Rifle)
	{
		Rifle->Trigger_Pulled();
	}
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageApplied = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	Deal_Damage(DamageApplied); 

	if(Is_Dead())
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
		if(GameMode)
		{
			GameMode->Pawn_Killed(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageApplied;
}
void AShooterCharacter::Deal_Damage(float Damage)
{
	Health -= Damage;
	Health = FMath::Clamp<int32> (Health, 0, MaxHealth);

	UE_LOG(LogTemp, Warning, TEXT("%i"), Health);
}
